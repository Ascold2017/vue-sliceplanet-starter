import axios from 'axios'

// import store from '@/store'
// import router from '@/router'
// import qs from 'qs'

const request = axios.create({
  baseURL: '',
  headers: {
    'Authorization': ''
  }
})

export function getExample (lang) {
  return request.get(`/${lang}/api/`)
    .then(res => res.data)
    .catch(e => { throw new Error(e.response.data.message || e) })
}
