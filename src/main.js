import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import VueCookie from 'vue-cookie'
import VueMeta from 'vue-meta'

import store from '@/store'
import router from '@/router'

import App from '@/App'

sync(store, router)

Vue.use(VueCookie)
Vue.use(VueMeta)

Vue.config.productionTip = false

const $app = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

export default $app
