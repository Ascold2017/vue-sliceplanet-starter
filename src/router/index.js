import Vue from 'vue'
import Router from 'vue-router'
// import store from '@/store'

Vue.use(Router)

const route = (componentName, path, name) => ({
  path,
  name,
  component: () => import(`@/components/${componentName}`)
})

export default new Router({
  // mode: 'history',

  // reset position
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },

  routes: [
    route('Home', '/', 'home'),
    route('About', '/about', 'about')
  ]
})
